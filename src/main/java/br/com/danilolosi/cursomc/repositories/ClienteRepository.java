package br.com.danilolosi.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.danilolosi.cursomc.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

}
