package br.com.danilolosi.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.danilolosi.cursomc.domain.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Integer>{

}
