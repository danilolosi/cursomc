package br.com.danilolosi.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.danilolosi.cursomc.domain.Pagamento;

public interface PagamentoRepository extends JpaRepository<Pagamento, Integer> {

}
