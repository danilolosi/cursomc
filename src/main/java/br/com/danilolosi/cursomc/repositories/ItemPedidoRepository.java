package br.com.danilolosi.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.danilolosi.cursomc.domain.ItemPedido;

public interface ItemPedidoRepository extends JpaRepository<ItemPedido, Integer> {

}
